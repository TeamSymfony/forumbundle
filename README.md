﻿ForumBundle - Symfony2
========================

Forum Bundle est un bundle crée pour Symfony2 qui permet d'ajouter à un projet un forum.

-------------------------------


OPTIMISATIONS EVENTUELLES
===========================

Contrôler la façon dont on créé un sujet avec un message lié, privilégier un formulaire imbriqué si c'est plus simple.
https://bitbucket.org/TeamSymfony/forumbundle/commits/cb02889ec86bccb25ac362a4921a9a9e#comment-636413


TODO LIST & FONCTIONS DU FORUM
===========================
* Créer des topics
* Ajouter des posts
* Editer des posts
* Répondre à un post
* Modération
* Vote sur message
* Support du BBCODE (ou code du même type)
* Traduction FR/EN
* Block derniers topics/messages
* Nombre de messages/topics

-----------------------------------



ENTITY
================================

* Ajouter des entity

---------------------------------



Idées
===============================

Créer un GroupBundle