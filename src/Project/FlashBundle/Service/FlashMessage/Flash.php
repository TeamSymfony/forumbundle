<?php
 
namespace Project\FlashBundle\Service\FlashMessage;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
/*
 * Service pour utiliser les flash avec moin de code.
 * Utilisation du service : 
 * $flash = $this->container->get('project.flash');  (Call du service)
 * $flash->putFlash('Type de flash', 'Message du flash');
 * Si aucun message de flash n'est envoyé au service,
 * on essaye de placer un message prédefini en fonction du flashtype.
 * Pour ajouter un nouveau message, il suffit d'ajouter $flashtype == 'nom du type de flash'
 * dans les if qui comportent déjà les param de base.
 * Il suffit ensuite d'ajouter sa condition dans le switch.
 * 
 * /!\ CE SERVICE NE FAIT AUCUN RETURN /!\
 */
class Flash
{
    protected $request;
 
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
 
    public function putFlash($flashtype = null, $message = null)
    {
        if ($flashtype == null) { // Forcer l'utilisation d'un type de flash
            throw new \Exception('Erreur, aucun type de flash bag (error, success, info) passé au service');
        }
         
        // A utiliser uniquement si besoin d'utiliser des types de flash strict
        /*if ($flashtype !== 'error' || $flashtype !== 'success' || $flashtype !== 'info') {
            throw new \Exception('Type de flash invalide (error, success, info)');
        }*/
 
        // Si aucun message n'est utilisé, essayer de lui assigner un message par défaut
        if ($message == null) {
            if ($flashtype == 'error' || $flashtype == 'success' || $flashtype == 'info') {
                $msg_error = 'Une erreur c\'est produite.';
                $msg_success = 'L\'action a été réalisé avec succès !';
                $msg_info = 'Veuillez modifier ce message.';
 
                switch ($flashtype) {
                    case 'error': $message = $msg_error; break;
                    case 'success': $message = $msg_success; break;
                    case 'info': $message = $msg_info; break;
                    default :
                        $message = 'Aucun message par défaut disponible pour ce type de flash, merci d\'ajouter un message (second param.)';
                }
            }          
        }
 
        // Puis met le flash dans la session de l'utilisateur
        $session = $this->request->getSession();
        $session->getFlashBag()->add($flashtype, $message);
    }
 
    // créé un message flash et renvois sur la page de provenance
    public function putFlashAndRedirect($flashtype = null, $message = null)
    {
        $this->putFlash($flashtype, $message);
        return new RedirectResponse($this->request->headers->get('referer'));
    }
}