<?php

namespace Project\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Project\UserBundle\Entity\Team;
use Project\UserBundle\Form\TeamType;

/**
 * Team controller.
 *
 * @Route("/admin/teams")
 */
class TeamAdminController extends Controller
{

    /**
     * Lists all Team entities.
     *
     * @Route("/", name="admin_team")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $teams = $em->getRepository('ProjectUserBundle:Team')->findAll();

        return array(
            'teams' => $teams,
        );
    }
    /**
     * Creates a new Team entity.
     *
     * @Route("/", name="admin_team_create")
     * @Method("POST")
     * @Template("ProjectUserBundle:Team:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $team  = new Team();
        $form = $this->createForm(new TeamType(), $team);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($team);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_team_show', array('id' => $team->getId())));
        }

        return array(
            'team' => $team,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Team entity.
     *
     * @Route("/new", name="admin_team_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $team = new Team();
        $form   = $this->createForm(new TeamType(), $team);

        return array(
            'team' => $team,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Team entity.
     *
     * @Route("/{id}", name="admin_team_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $team = $em->getRepository('ProjectUserBundle:Team')->find($id);

        if (!$team) {
            throw $this->createNotFoundException('Unable to find Team entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'team'      => $team,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Team entity.
     *
     * @Route("/{id}/edit", name="admin_team_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $team = $em->getRepository('ProjectUserBundle:Team')->find($id);

        if (!$team) {
            throw $this->createNotFoundException('Unable to find Team entity.');
        }

        $editForm = $this->createForm(new TeamType(), $team);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'team'      => $team,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Team entity.
     *
     * @Route("/{id}", name="admin_team_update")
     * @Method("PUT")
     * @Template("ProjectUserBundle:Team:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $team = $em->getRepository('ProjectUserBundle:Team')->find($id);

        if (!$team) {
            throw $this->createNotFoundException('Unable to find Team entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new TeamType(), $team);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($team);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_team_edit', array('id' => $id)));
        }

        return array(
            'team'      => $team,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Team entity.
     *
     * @Route("/{id}", name="admin_team_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $team = $em->getRepository('ProjectUserBundle:Team')->find($id);

            if (!$team) {
                throw $this->createNotFoundException('Unable to find Team entity.');
            }

            $em->remove($team);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_team'));
    }

    /**
     * Creates a form to delete a Team entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
