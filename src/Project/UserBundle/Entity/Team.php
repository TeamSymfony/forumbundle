<?php

namespace Project\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Team
 *
 * @ORM\Table(name="team")
 * @ORM\Entity(repositoryClass="Project\UserBundle\Entity\TeamRepository")
 */
class Team
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->forums = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @ORM\ManyToMany(targetEntity="Project\UserBundle\Entity\User", inversedBy="teams")
     **/
    private $users;

    /**
     * @ORM\ManyToMany(targetEntity="Project\ForumBundle\Entity\Category", inversedBy="groupsWithAccess")
     **/
    private $categories;

    /**
     * @ORM\ManyToMany(targetEntity="Project\ForumBundle\Entity\Forum", inversedBy="groupsWithAccess")
     **/
    private $forums;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="teamquote", type="string", length=255)
     */
    private $teamquote;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Team
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Team
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set teamquote
     *
     * @param string $teamquote
     * @return Team
     */
    public function setTeamquote($teamquote)
    {
        $this->teamquote = $teamquote;
    
        return $this;
    }

    /**
     * Get teamquote
     *
     * @return string 
     */
    public function getTeamquote()
    {
        return $this->teamquote;
    }
    
    /**
     * Add users
     *
     * @param \Project\UserBundle\Entity\User $users
     * @return Team
     */
    public function addUser(\Project\UserBundle\Entity\User $users)
    {
        $this->users[] = $users;
    
        return $this;
    }

    /**
     * Remove users
     *
     * @param \Project\UserBundle\Entity\User $users
     */
    public function removeUser(\Project\UserBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add categories
     *
     * @param \Project\ForumBundle\Entity\Category $categories
     * @return Team
     */
    public function addCategorie(\Project\ForumBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;
    
        return $this;
    }

    /**
     * Remove categories
     *
     * @param \Project\ForumBundle\Entity\Category $categories
     */
    public function removeCategorie(\Project\ForumBundle\Entity\Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Add forums
     *
     * @param \Project\ForumBundle\Entity\Forum $forums
     * @return Team
     */
    public function addForum(\Project\ForumBundle\Entity\Forum $forums)
    {
        $this->forums[] = $forums;
    
        return $this;
    }

    /**
     * Remove forums
     *
     * @param \Project\ForumBundle\Entity\Forum $forums
     */
    public function removeForum(\Project\ForumBundle\Entity\Forum $forums)
    {
        $this->forums->removeElement($forums);
    }

    /**
     * Get forums
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getForums()
    {
        return $this->forums;
    }
}