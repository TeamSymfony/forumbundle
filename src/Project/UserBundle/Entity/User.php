<?php
 
namespace Project\UserBundle\Entity;
 
use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
 
/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->teams = new \Doctrine\Common\Collections\ArrayCollection();
        $this->threads = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @ORM\ManyToMany(targetEntity="Project\UserBundle\Entity\Team", mappedBy="users")
     **/
    protected $teams;

    /**
     * @ORM\OneToMany(targetEntity="Project\ForumBundle\Entity\Message", mappedBy="user")
     **/
    protected $messages;

    /**
     * @ORM\OneToMany(targetEntity="Project\ForumBundle\Entity\Thread", mappedBy="user")
     **/
    protected $threads;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add teams
     *
     * @param \Project\UserBundle\Entity\Team $teams
     * @return User
     */
    public function addTeam(\Project\UserBundle\Entity\Team $teams)
    {
        $this->teams[] = $teams;
    
        return $this;
    }

    /**
     * Remove teams
     *
     * @param \Project\UserBundle\Entity\Team $teams
     */
    public function removeTeam(\Project\UserBundle\Entity\Team $teams)
    {
        $this->teams->removeElement($teams);
    }

    /**
     * Get teams
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTeams()
    {
        return $this->teams;
    }

    /**
     * Add messages
     *
     * @param \Project\ForumBundle\Entity\Message $messages
     * @return User
     */
    public function addMessage(\Project\ForumBundle\Entity\Message $messages)
    {
        $this->messages[] = $messages;
    
        return $this;
    }

    /**
     * Remove messages
     *
     * @param \Project\ForumBundle\Entity\Message $messages
     */
    public function removeMessage(\Project\ForumBundle\Entity\Message $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Add threads
     *
     * @param \Project\ForumBundle\Entity\Thread $threads
     * @return User
     */
    public function addThread(\Project\ForumBundle\Entity\Thread $threads)
    {
        $this->threads[] = $threads;
    
        return $this;
    }

    /**
     * Remove threads
     *
     * @param \Project\ForumBundle\Entity\Thread $threads
     */
    public function removeThread(\Project\ForumBundle\Entity\Thread $threads)
    {
        $this->threads->removeElement($threads);
    }

    /**
     * Get threads
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getThreads()
    {
        return $this->threads;
    }
}