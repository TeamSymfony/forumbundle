<?php
 
namespace Project\ForumBundle\Service\Message;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Project\ForumBundle\Entity\Forum;
use Project\ForumBundle\Entity\Thread;
use Project\ForumBundle\Entity\Message;
use Project\FlashBundle\Service\FlashMessage\Flash;
use Project\ForumBundle\Service\Parser\BBcodeParser;

class CreateMessage
{
    public $message;
    public $thread;
    public $forum;
    
    public $flashService; // notre service pour l'affichage de messages d'erreurs, d'info, etc.
    public $bbcodeParser; // notre service pour le parsage du bbcode.

    public $categoryNodeId;
    public $currentUser;
    public $securityContext;
    public $request;
    public $em; // entityManager

    private function verifyIfUserIsAuthentificated($errorMessage)
    {
        if(!$this->securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')){
            $this->flashService->putFlashAndRedirect('error', $errorMessage);
        }
    }

    public function __construct(Flash $flashService, BBcodeParser $bbcodeParser, securityContext $securityContext, Request $request, EntityManager $entityManager)
    {
        $this->bbcodeParser = $bbcodeParser;
        $this->flashService = $flashService;
        $this->securityContext = $securityContext;
        $this->currentUser = $securityContext->getToken()->getUser();
        $this->request = $request;
        $this->em = $entityManager;
    }

    public function createThreadAndFirstMessage(Message $message, Thread $thread, Forum $forum)
    {
        $this->verifyIfUserIsAuthentificated('Vous devez être en ligne pour créer un sujet.');

        // On hydrate l'objet Thread avec les informations manquantes.

        $thread->setUser($this->currentUser);

        $thread->setForum($forum); // lie le sujet au forum auquel il dois appartenir
        $thread->setCategoryNodeId($forum->getCategoryNodeId());
        $thread->setFirstPostUsername($this->currentUser->getUsername());
        $thread->setFirstPostUserId($this->currentUser->getId());
        $thread->setFirstPostId(0); // Valeur temporaire le temps qu'un id soit généré.

        $forum->setDiscussionCount($forum->getDiscussionCount()+1); // +1 sujet au forum

        $this->createMessage($message, $thread, $forum);
    }

    public function createMessage(Message $message, Thread $thread, Forum $forum)
    {
        $this->verifyIfUserIsAuthentificated('Vous devez être en ligne pour créer un message.');

        // MAJ Thread
        $thread->setLastPostUserId($this->currentUser->getId());
        $thread->setLastPostUsername($this->currentUser->getUsername());
        $thread->setReplyCount($thread->getReplyCount()+1);
        $thread->setLastPostId(0); // Valeur temporaire le temps qu'un id soit généré.

        $this->thread = $thread;

        // MAJ Message

        // On lie le message au sujet
        $message->setThread($this->thread);

        // On parse le message pour passer du BBcode au HTML
        $message->setContent($this->bbcodeParser->parseToHtml($message->getContent())); 

        $message->setCategoryNodeId($thread->getCategoryNodeId());
        $message->setIp($this->request->getClientIp());
        $message->setUser($this->currentUser);

        $this->message = $message;

        // MAJ Forum
        $forum->setMessageCount($forum->getMessageCount()+1);
        $forum->setLastPostDate($this->message->getDateCreation());
        $forum->setLastPostUserId($this->currentUser->getId());
        $forum->setLastPostUsername($this->currentUser->getUsername());
        $forum->setLastThreadTitle($this->thread->getTitle());
        $forum->setLastPostId(0); // Valeur temporaire le temps qu'un id soit généré.
        $forum->setLastThreadId(0); // Valeur temporaire le temps qu'un id soit généré.

        $this->forum = $forum;

        // On enregistre
        $this->em->persist($this->message);
        $this->em->persist($this->thread);
        $this->em->persist($this->forum);
        $this->em->flush();

        // On finalise on mettant à jour les informations requièrent les id précédement indisponible car non présebt en bdd
        $this->sendDataAndFinalize();

    }

    private function sendDataAndFinalize()
    {
        $this->forum->setLastPostId($this->message->getId());
        $this->forum->setLastThreadId($this->thread->getId()); // dernier sujet mis à jour et non créé
        $this->thread->setLastPostId($this->message->getId());

        if($this->thread->getFirstPostId() == 0)
        {
            $this->thread->setFirstPostId($this->message->getId());
        }

        $this->em->persist($this->thread);
        $this->em->persist($this->forum);
        $this->em->flush();

        $this->flashService->putFlash('success', 'Votre message a créé avec succès.');
    }
}