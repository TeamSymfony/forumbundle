<?php

namespace Project\ForumBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Project\ForumBundle\Entity\Category;
use Project\ForumBundle\Form\CategoryType;

use Project\ForumBundle\Entity\Forum;

/**
 * Category controller.
 *
 * @Route("/")
 */
class CategoryController extends Controller
{
    /**
     * Lists all Category entities.
     *
     * @Route("/", name="category")
     * @Method("GET")
     * @Template("ProjectForumBundle:Public:Category/index.html.twig")
     */
    public function indexAction()
    {
        $flash = $this->container->get('project.flash');
        $flash->putFlash('info', 'Locale utilisateur: '.$this->container->getParameter('locale'));

        $em = $this->getDoctrine()->getManager();
        $rp = $em->getRepository('ProjectForumBundle:Category');
        $categories = $rp->getCategories();
        
        return array(
            'categories' => $categories,
        );
    }
}
