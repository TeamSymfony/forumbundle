<?php

namespace Project\ForumBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Project\ForumBundle\Entity\Message;
use Project\ForumBundle\Entity\Thread;
use Project\ForumBundle\Form\MessageType;

use Symfony\Component\HttpFoundation\Response;

/**
 * Message controller.
 *
 * @Route("/message")
 */
class MessageController extends Controller
{
    private function getCurrentUser()
    { return $this->container->get('security.context')->getToken()->getUser(); }

    private function verifyIfUserIsAuthentificated($errorMessage)
    {
        if(!$this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')){
            $flash = $this->container->get('project.flash');
            return $flash->putFlashAndRedirect('error', $errorMessage);
        }
    }

    /**
     * @Route("/", name="bbcodeParser")
     * @Method("POST")
     */
    public function parserAction(Request $request)
    {
        $bbcodeMessage = $request->get('data');

        $parser = $this->container->get('forum.parser');
        $messageParsed = $parser->parseToHtml($bbcodeMessage);

        return new Response($messageParsed);
    }

    /**
     * Lists all Message messages.
     *
     * @Route("/", name="message")
     * @Method("GET")
     * @Template("ProjectForumBundle:Public:Message/index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $messages = $em->getRepository('ProjectForumBundle:Message')->findAll();

        return array(
            'messages' => $messages,
        );
    }

    /**
     * Creates a new Message entity.
     *
     * @Route("/create/{thread}", name="message_create")
     * @Method("POST")
     * @Template("ProjectForumBundle:Public:Message/new.html.twig")
     */
    public function createAction(Request $request, Thread $thread)
    {
        $message  = new Message();
        $form = $this->createForm(new MessageType(), $message);
        $form->bind($request);

        if ($form->isValid()) {

            // On créé le message et le sujet et on met le tout à jour (message, thread, forum) dans la BDD.
            $service = $this->container->get('forum.createmessage');
            $service->createMessage($message, $thread, $thread->getForum());
            $thread  = $service->thread;

            return $this->redirect($this->generateUrl('message_show', array('id' => $message->getId())));
        }

        return array(
            'message' => $message,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Message entity.
     *
     * @Route("/new", name="message_new")
     * @Method("GET")
     * @Template("ProjectForumBundle:Public:Message/new.html.twig")
     */
    public function newAction()
    {
        $this->verifyIfUserIsAuthentificated('Vous devez être en ligne pour créer un message');

        $message = new Message();
        $form   = $this->createForm(new MessageType(), $message);

        return array(
            'message' => $message,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Message entity.
     *
     * @Route("/{id}", name="message_show")
     * @Method("GET")
     * @Template("ProjectForumBundle:Public:Message/show.html.twig")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $message = $em->getRepository('ProjectForumBundle:Message')->find($id);

        if (!$message) {
            throw $this->createNotFoundException('Unable to find Message entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'message'      => $message,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Message entity.
     *
     * @Route("/{id}/edit", name="message_edit")
     * @Method("GET")
     * @Template("ProjectForumBundle:Public:Message/edit.html.twig")
     */
    public function editAction($id)
    {
        $this->verifyIfUserIsAuthentificated('Vous devez être en ligne pour modifier un message');

        $em = $this->getDoctrine()->getManager();

        $message = $em->getRepository('ProjectForumBundle:Message')->find($id);

        if (!$message) {
            throw $this->createNotFoundException('Unable to find Message entity.');
        }

        $editForm = $this->createForm(new MessageType(), $message);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'message'      => $message,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Message entity.
     *
     * @Route("/{id}", name="message_update")
     * @Method("PUT")
     * @Template("ProjectForumBundle:Public:Message/edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $this->verifyIfUserIsAuthentificated('Vous devez être en ligne pour modifier un message');

        $em = $this->getDoctrine()->getManager();

        $message = $em->getRepository('ProjectForumBundle:Message')->find($id);

        if (!$message) {
            throw $this->createNotFoundException('Unable to find Message entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new MessageType(), $message);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($message);
            $em->flush();

            return $this->redirect($this->generateUrl('message_edit', array('id' => $id)));
        }

        return array(
            'message'      => $message,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Message entity.
     *
     * @Route("/{id}", name="message_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->verifyIfUserIsAuthentificated('Vous devez être en ligne pour supprimer un message');

        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $message = $em->getRepository('ProjectForumBundle:Message')->find($id);

            if (!$message) {
                throw $this->createNotFoundException('Unable to find Message entity.');
            }

            $em->remove($message);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('message'));
    }

    /**
     * Creates a form to delete a Message entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
