<?php

namespace Project\ForumBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Project\ForumBundle\Entity\Forum;
use Project\ForumBundle\Entity\Thread;
use Project\ForumBundle\Entity\Message;
use Project\ForumBundle\Form\ThreadType;
use Project\ForumBundle\Form\MessageType;
use Project\ForumBundle\Form\NewthreadType;

/**
 * Thread controller.
 *
 * @Route("/thread")
 */
class ThreadController extends Controller
{
    private function getCurrentUser()
    { return $this->container->get('security.context')->getToken()->getUser(); }

    private function verifyIfUserIsAuthentificated($errorMessage)
    {
        if(!$this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')){
            $flash = $this->container->get('project.flash');
            return $flash->putFlashAndRedirect('error', $errorMessage);
        }
    }

    /**
     * Lists all Thread entities.
     *
     * @Route("/", name="thread")
     * @Method("GET")
     * @Template("ProjectForumBundle:Public:Thread/index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $threads = $em->getRepository('ProjectForumBundle:Thread')->findAll();

        return array(
            'threads' => $threads,
        );
    }

    /**
     * Creates a new Thread entity.
     *
     * @Route("/{forum}", name="thread_create")
     * @Method("POST")
     * @Template("ProjectForumBundle:Public:Thread/new.html.twig")
     */
    public function createAction(Request $request, Forum $forum)
    {
        $this->verifyIfUserIsAuthentificated('Vous devez être en ligne pour créer un sujet');

        $message = new Message();

        // Retourne un nouveau message avec un nouveau sujet lié à ce même message.
        $form = $this->createForm(new NewthreadType(), $message);
        $form->bind($request);
        $thread = $message->getThread(); // le message a automatiquement d'associé un sujet grace au formType

        // IMPORTANT: CONTROLLER DANS LE SERVICE SI L'USER A LE DROIT DE POSTER DANS LE FORUM

        if ($form->isValid())
        {
            // On créé le message et le sujet et on met le tout à jour (message, thread, forum) dans la BDD.
            $service = $this->container->get('forum.createmessage');
            $service->createThreadAndFirstMessage($message, $thread, $forum);
            $thread  = $service->thread;

            return $this->redirect($this->generateUrl('thread_show', array('thread' => $thread->getId())));
        }

        return array(
            'forumId' => $forum->getId(),
            'thread' => $thread,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Thread entity.
     *
     * @Route("/forum-{forumId}/new", name="thread_new")
     * @Method("GET")
     * @Template("ProjectForumBundle:Public:Thread/new.html.twig")
     */
    public function newAction($forumId)
    {
        $this->verifyIfUserIsAuthentificated('Vous devez être en ligne pour créer un sujet');

        $message = new Message();
        $form = $this->createForm(new NewthreadType(), $message);

        return array(
            'forumId' => $forumId,
            'form'   => $form->createView(),
        );
    }





    /**
     * Finds and displays a Thread entity.
     *
     * @Route("/{thread}", name="thread_show")
     * @Method("GET")
     * @Template("ProjectForumBundle:Public:Thread/show.html.twig")
     */
    public function showAction($thread)
    {
        if(!$thread)
        {
            throw $this->createNotFoundException('Unable to find Thread entity.');
        }
        
        $em = $this->getDoctrine()->getManager();
        $rp = $em->getRepository('ProjectForumBundle:Thread');
        $thread = $rp->getThread($thread);

        $message = new Message();
        $newMessage = $this->createForm(new MessageType(), $message);

        $breadcrumbThreadTitle = $thread[0]->getTitle();
        $breadcrumbForumId = $thread['parentForumId'];
        $breadcrumbForumTitle = $thread['parentForumTitle'];
        $breadcrumbThreadId = $thread[0]->getId();

        $thread = $thread[0];
        $thread->setViewCount($thread->getViewCount()+1);

        $em->persist($thread);
        $em->flush();

        return array(
            'thread' => $thread,
            'breadcrumbThreadTitle' => $breadcrumbThreadTitle,
            'breadcrumbForumId' => $breadcrumbForumId,
            'breadcrumbForumTitle' => $breadcrumbForumTitle,
            'breadcrumbThreadId' => $breadcrumbThreadId,
            'form' => $newMessage->createView(),
        );
    }










    /**
     * Displays a form to edit an existing Thread entity.
     *
     * @Route("/{threadId}/edit", name="thread_edit")
     * @Method("GET")
     * @Template("ProjectForumBundle:Public:Thread/edit.html.twig")
     */
    public function editAction($threadId)
    {
        $this->verifyIfUserIsAuthentificated('Vous devez être en ligne pour modifier un sujet');

        $em = $this->getDoctrine()->getManager();

        $thread = $em->getRepository('ProjectForumBundle:Thread')->find($threadId);

        if (!$thread) {
            throw $this->createNotFoundException('Unable to find Thread entity.');
        }

        $editForm = $this->createForm(new ThreadType(), $thread);
        $deleteForm = $this->createDeleteForm($threadId);

        return array(
            'thread'      => $thread,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Thread entity.
     *
     * @Route("/{threadId}", name="thread_update")
     * @Method("PUT")
     * @Template("ProjectForumBundle:Public:Thread/edit.html.twig")
     */
    public function updateAction(Request $request, $threadId)
    {
        $this->verifyIfUserIsAuthentificated('Vous devez être en ligne pour modifier un sujet');

        $em = $this->getDoctrine()->getManager();

        $thread = $em->getRepository('ProjectForumBundle:Thread')->find($threadId);

        if (!$thread) {
            throw $this->createNotFoundException('Unable to find Thread entity.');
        }

        $deleteForm = $this->createDeleteForm($threadId);
        $editForm = $this->createForm(new ThreadType(), $thread);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($thread);
            $em->flush();

            return $this->redirect($this->generateUrl('thread_edit', array('threadId' => $threadId)));
        }

        return array(
            'thread'      => $thread,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Thread entity.
     *
     * @Route("/{threadId}", name="thread_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $threadId)
    {
        $this->verifyIfUserIsAuthentificated('Vous devez être en ligne pour supprimer un sujet');

        $form = $this->createDeleteForm($threadId);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $thread = $em->getRepository('ProjectForumBundle:Thread')->find($threadId);

            if (!$thread) {
                throw $this->createNotFoundException('Unable to find Thread entity.');
            }

            $em->remove($thread);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('thread'));
    }

    /**
     * Creates a form to delete a Thread entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($threadId)
    {
        return $this->createFormBuilder(array('id' => $threadId))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
