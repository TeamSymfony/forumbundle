<?php

namespace Project\ForumBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Project\ForumBundle\Entity\Forum;
use Project\ForumBundle\Form\ForumType;

/**
 * Forum controller.
 *
 * @Route("/admin/forums")
 */
class ForumAdminController extends Controller
{

    /**
     * Lists all Forum entities.
     *
     * @Route("/", name="admin_forum")
     * @Method("GET")
     * @Template("ProjectForumBundle:Admin:Forum/index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $forums = $em->getRepository('ProjectForumBundle:Forum')->findAll();

        return array(
            'forums' => $forums,
        );
    }
    /**
     * Creates a new Forum entity.
     *
     * @Route("/", name="admin_forum_create")
     * @Method("POST")
     * @Template("ProjectForumBundle:Admin:Forum/new.html.twig")
     */
    public function createAction(Request $request)
    {
        $forum  = new Forum();
        $form = $this->createForm(new ForumType(), $forum);
        $form->bind($request);

        $forum->setCategoryNodeId($forum->getCategory()->getId());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($forum);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_forum_show', array('id' => $forum->getId())));
        }

        return array(
            'forum' => $forum,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Forum entity.
     *
     * @Route("/new", name="admin_forum_new")
     * @Method("GET")
     * @Template("ProjectForumBundle:Admin:Forum/new.html.twig")
     */
    public function newAction()
    {
        $forum = new Forum();
        $form   = $this->createForm(new ForumType(), $forum);

        return array(
            'forum' => $forum,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Forum entity.
     *
     * @Route("/{id}", name="admin_forum_show")
     * @Method("GET")
     * @Template("ProjectForumBundle:Admin:Forum/show.html.twig")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $forum = $em->getRepository('ProjectForumBundle:Forum')->find($id);

        if (!$forum) {
            throw $this->createNotFoundException('Unable to find Forum entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'forum'      => $forum,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Forum entity.
     *
     * @Route("/{id}/edit", name="admin_forum_edit")
     * @Method("GET")
     * @Template("ProjectForumBundle:Admin:Forum/edit.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $forum = $em->getRepository('ProjectForumBundle:Forum')->find($id);

        if (!$forum) {
            throw $this->createNotFoundException('Unable to find Forum entity.');
        }

        $editForm = $this->createForm(new ForumType(), $forum);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'forum'      => $forum,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Forum entity.
     *
     * @Route("/{id}", name="admin_forum_update")
     * @Method("PUT")
     * @Template("ProjectForumBundle:Admin:Forum/edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $forum = $em->getRepository('ProjectForumBundle:Forum')->find($id);

        if (!$forum) {
            throw $this->createNotFoundException('Unable to find Forum entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ForumType(), $forum);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($forum);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_forum_edit', array('id' => $id)));
        }

        return array(
            'forum'      => $forum,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Forum entity.
     *
     * @Route("/{id}", name="admin_forum_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $forum = $em->getRepository('ProjectForumBundle:Forum')->find($id);

            if (!$forum) {
                throw $this->createNotFoundException('Unable to find Forum entity.');
            }

            $em->remove($forum);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_forum'));
    }

    /**
     * Creates a form to delete a Forum entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
