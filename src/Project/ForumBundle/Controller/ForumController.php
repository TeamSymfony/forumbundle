<?php

namespace Project\ForumBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Project\ForumBundle\Entity\Forum;
use Project\ForumBundle\Form\ForumType;

/**
 * Forum controller.
 *
 * @Route("/forum")
 */
class ForumController extends Controller
{
    /**
     * Finds and displays a Forum entity.
     *
     * @Route("/{forumId}", name="forum_show")
     * @Method("GET")
     * @Template("ProjectForumBundle:Public:Forum/show.html.twig")
     */
    public function showAction($forumId)
    {
        $em = $this->getDoctrine()->getManager();
        $rp = $em->getRepository('ProjectForumBundle:Forum');

        $forum = $rp->getForum($forumId);
        $forumTitle = $forum->getTitle();

        return array(
            'forum' => $forum,
            'breadcrumbForumId' => $forumId,
            'breadcrumbForumTitle' => $forumTitle
        );
    }
}
