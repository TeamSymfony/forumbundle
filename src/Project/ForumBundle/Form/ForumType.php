<?php

namespace Project\ForumBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ForumType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array('required'  => true, 'attr' => array('autocomplete' => 'off')))
            ->add('description', 'text', array('required'  => false, 'attr' => array('autocomplete' => 'off')))
            ->add('displayOrder', 'number', array('required'  => true, 'attr' => array('autocomplete' => 'off')))
            ->add('category', 'entity', array(
                'class' => 'ProjectForumBundle:Category',
                'property' => 'title',
                ))
            ->add('isLocked', 'checkbox', array('required'  => false))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Project\ForumBundle\Entity\Forum'
        ));
    }

    public function getName()
    {
        return 'project_forumbundle_forumtype';
    }
}
