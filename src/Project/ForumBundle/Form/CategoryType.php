<?php

namespace Project\ForumBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array('required'  => true, 'attr' => array('autocomplete' => 'off')))
            ->add('description', 'text', array('required'  => false, 'attr' => array('autocomplete' => 'off')))
            ->add('displayOrder', 'number')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Project\ForumBundle\Entity\Category'
        ));
    }

    public function getName()
    {
        return 'project_forumbundle_categorytype';
    }
}
