<?php

namespace Project\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Project\ForumBundle\Entity\CategoryRepository")
 */
class Category
{
    public function __construct()
    {
        $this->displayOrder = 0;
        $this->forums = new \Doctrine\Common\Collections\ArrayCollection();
        $this->groupsWithAccess = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @ORM\OneToMany(targetEntity="Project\ForumBundle\Entity\Forum", mappedBy="category", cascade={"remove"})
     */
    private $forums;

    /**
     * @ORM\ManyToMany(targetEntity="Project\UserBundle\Entity\Team", mappedBy="categories")
     **/
    private $groupsWithAccess;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="display_order", type="integer")
     */
    private $displayOrder;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Category
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Category
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set displayOrder
     *
     * @param integer $displayOrder
     * @return Category
     */
    public function setDisplayOrder($displayOrder)
    {
        $this->displayOrder = $displayOrder;
    
        return $this;
    }

    /**
     * Get displayOrder
     *
     * @return integer 
     */
    public function getDisplayOrder()
    {
        return $this->displayOrder;
    }

    /**
     * Add forums
     *
     * @param \Project\ForumBundle\Entity\Forum $forums
     * @return Category
     */
    public function addForum(\Project\ForumBundle\Entity\Forum $forums)
    {
        $this->forums[] = $forums;
    
        return $this;
    }

    /**
     * Remove forums
     *
     * @param \Project\ForumBundle\Entity\Forum $forums
     */
    public function removeForum(\Project\ForumBundle\Entity\Forum $forums)
    {
        $this->forums->removeElement($forums);
    }

    /**
     * Get forums
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getForums()
    {
        return $this->forums;
    }

    /**
     * Add groupsWithAccess
     *
     * @param \Project\UserBundle\Entity\Team $groupsWithAccess
     * @return Category
     */
    public function addGroupsWithAcces(\Project\UserBundle\Entity\Team $groupsWithAccess)
    {
        $this->groupsWithAccess[] = $groupsWithAccess;
    
        return $this;
    }

    /**
     * Remove groupsWithAccess
     *
     * @param \Project\UserBundle\Entity\Team $groupsWithAccess
     */
    public function removeGroupsWithAcces(\Project\UserBundle\Entity\Team $groupsWithAccess)
    {
        $this->groupsWithAccess->removeElement($groupsWithAccess);
    }

    /**
     * Get groupsWithAccess
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroupsWithAccess()
    {
        return $this->groupsWithAccess;
    }
}