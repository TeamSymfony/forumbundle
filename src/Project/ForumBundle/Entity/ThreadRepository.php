<?php

namespace Project\ForumBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ThreadRepository extends EntityRepository
{
    public function getThread($threadId)
    {
      $qb = $this->createQueryBuilder('t');
      $qb->where('t.id = :id')
         ->setParameter('id', $threadId)
         ->leftJoin('t.forum', 'f')
         ->addSelect('f.id as parentForumId, f.title as parentForumTitle')
         ->leftJoin('t.messages', 'm')
         ->addSelect('m')
         ;

      return $qb->getQuery()->getSingleResult();
    }
}