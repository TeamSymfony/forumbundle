<?php

namespace Project\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Thread
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Project\ForumBundle\Entity\ThreadRepository")
 */
class Thread
{
    public function __construct()
    {
       $now = new \DateTime();

       $this->discussionOpen = true;
       $this->dateCreation = $now;
       $this->dateUpdate = $now;
       $this->lastPostDate = $now;
       $this->replyCount = 0;
       $this->viewCount = 0;
       $this->sticky = false;
       $this->isLocked = false;
       $this->firstPostLikes = 0;
       $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @ORM\ManyToOne(targetEntity="Project\ForumBundle\Entity\Forum", inversedBy="threads")
     */
    private $forum;

    /**
     * @ORM\OneToMany(targetEntity="Project\ForumBundle\Entity\Message", mappedBy="thread", cascade={"remove"})
     */
    private $messages;

    /**
     * @ORM\ManyToOne(targetEntity="Project\UserBundle\Entity\User", inversedBy="threads")
     **/
    protected $user;


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="category_node_id ", type="integer")
     */
    private $categoryNodeId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_locked", type="boolean")
     */
    private $isLocked;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_update", type="datetime")
     */
    private $dateUpdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="reply_count", type="integer")
     */
    private $replyCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="view_count", type="integer")
     */
    private $viewCount;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sticky", type="boolean")
     */
    private $sticky;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_post_date", type="datetime")
     */
    private $lastPostDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_post_id", type="integer")
     */
    private $lastPostId;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_post_user_id", type="integer")
     */
    private $lastPostUserId;

    /**
     * @var string
     *
     * @ORM\Column(name="last_post_username", type="string", length=255)
     */
    private $lastPostUsername;

    /**
     * @var integer
     *
     * @ORM\Column(name="first_post_id", type="integer")
     */
    private $firstPostId;

    /**
     * @var integer
     *
     * @ORM\Column(name="first_post_username", type="string", length=255)
     */
    private $firstPostUsername;

    /**
     * @var integer
     *
     * @ORM\Column(name="first_post_user_id", type="integer")
     */
    private $firstPostUserId;

    /**
     * @var integer
     *
     * @ORM\Column(name="first_post_likes", type="integer")
     */
    private $firstPostLikes;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Thread
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set categoryNodeId
     *
     * @param integer $categoryNodeId
     * @return Thread
     */
    public function setCategoryNodeId($categoryNodeId)
    {
        $this->categoryNodeId = $categoryNodeId;
    
        return $this;
    }

    /**
     * Get categoryNodeId
     *
     * @return integer 
     */
    public function getCategoryNodeId()
    {
        return $this->categoryNodeId;
    }

    /**
     * Set isLocked
     *
     * @param boolean $isLocked
     * @return Thread
     */
    public function setIsLocked($isLocked)
    {
        $this->isLocked = $isLocked;
    
        return $this;
    }

    /**
     * Get isLocked
     *
     * @return boolean 
     */
    public function getIsLocked()
    {
        return $this->isLocked;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     * @return Thread
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    
        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime 
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     * @return Thread
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;
    
        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime 
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set replyCount
     *
     * @param integer $replyCount
     * @return Thread
     */
    public function setReplyCount($replyCount)
    {
        $this->replyCount = $replyCount;
    
        return $this;
    }

    /**
     * Get replyCount
     *
     * @return integer 
     */
    public function getReplyCount()
    {
        return $this->replyCount;
    }

    /**
     * Set viewCount
     *
     * @param integer $viewCount
     * @return Thread
     */
    public function setViewCount($viewCount)
    {
        $this->viewCount = $viewCount;
    
        return $this;
    }

    /**
     * Get viewCount
     *
     * @return integer 
     */
    public function getViewCount()
    {
        return $this->viewCount;
    }

    /**
     * Set sticky
     *
     * @param boolean $sticky
     * @return Thread
     */
    public function setSticky($sticky)
    {
        $this->sticky = $sticky;
    
        return $this;
    }

    /**
     * Get sticky
     *
     * @return boolean 
     */
    public function getSticky()
    {
        return $this->sticky;
    }

    /**
     * Set lastPostDate
     *
     * @param \DateTime $lastPostDate
     * @return Thread
     */
    public function setLastPostDate($lastPostDate)
    {
        $this->lastPostDate = $lastPostDate;
    
        return $this;
    }

    /**
     * Get lastPostDate
     *
     * @return \DateTime 
     */
    public function getLastPostDate()
    {
        return $this->lastPostDate;
    }

    /**
     * Set lastPostId
     *
     * @param integer $lastPostId
     * @return Thread
     */
    public function setLastPostId($lastPostId)
    {
        $this->lastPostId = $lastPostId;
    
        return $this;
    }

    /**
     * Get lastPostId
     *
     * @return integer 
     */
    public function getLastPostId()
    {
        return $this->lastPostId;
    }

    /**
     * Set lastPostUserId
     *
     * @param integer $lastPostUserId
     * @return Thread
     */
    public function setLastPostUserId($lastPostUserId)
    {
        $this->lastPostUserId = $lastPostUserId;
    
        return $this;
    }

    /**
     * Get lastPostUserId
     *
     * @return integer 
     */
    public function getLastPostUserId()
    {
        return $this->lastPostUserId;
    }

    /**
     * Set lastPostUsername
     *
     * @param string $lastPostUsername
     * @return Thread
     */
    public function setLastPostUsername($lastPostUsername)
    {
        $this->lastPostUsername = $lastPostUsername;
    
        return $this;
    }

    /**
     * Get lastPostUsername
     *
     * @return string 
     */
    public function getLastPostUsername()
    {
        return $this->lastPostUsername;
    }

    /**
     * Set firstPostId
     *
     * @param integer $firstPostId
     * @return Thread
     */
    public function setFirstPostId($firstPostId)
    {
        $this->firstPostId = $firstPostId;
    
        return $this;
    }

    /**
     * Get firstPostId
     *
     * @return integer 
     */
    public function getFirstPostId()
    {
        return $this->firstPostId;
    }

    /**
     * Set firstPostLikes
     *
     * @param integer $firstPostLikes
     * @return Thread
     */
    public function setFirstPostLikes($firstPostLikes)
    {
        $this->firstPostLikes = $firstPostLikes;
    
        return $this;
    }

    /**
     * Get firstPostLikes
     *
     * @return integer 
     */
    public function getFirstPostLikes()
    {
        return $this->firstPostLikes;
    }

    /**
     * Add messages
     *
     * @param \Project\ForumBundle\Entity\Message $messages
     * @return Thread
     */
    public function addMessage(\Project\ForumBundle\Entity\Message $messages)
    {
        $this->messages[] = $messages;
    
        return $this;
    }

    /**
     * Remove messages
     *
     * @param \Project\ForumBundle\Entity\Message $messages
     */
    public function removeMessage(\Project\ForumBundle\Entity\Message $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Add forum
     *
     * @param \Project\ForumBundle\Entity\Forum $forum
     * @return Thread
     */
    public function addForum(\Project\ForumBundle\Entity\Forum $forum)
    {
        $this->forum[] = $forum;
    
        return $this;
    }

    /**
     * Remove forum
     *
     * @param \Project\ForumBundle\Entity\Forum $forum
     */
    public function removeForum(\Project\ForumBundle\Entity\Forum $forum)
    {
        $this->forum->removeElement($forum);
    }

    /**
     * Get forum
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getForum()
    {
        return $this->forum;
    }

    /**
     * Set firstPostUsername
     *
     * @param string $firstPostUsername
     * @return Thread
     */
    public function setFirstPostUsername($firstPostUsername)
    {
        $this->firstPostUsername = $firstPostUsername;
    
        return $this;
    }

    /**
     * Get firstPostUsername
     *
     * @return string 
     */
    public function getFirstPostUsername()
    {
        return $this->firstPostUsername;
    }

    /**
     * Set firstPostUserId
     *
     * @param integer $firstPostUserId
     * @return Thread
     */
    public function setFirstPostUserId($firstPostUserId)
    {
        $this->firstPostUserId = $firstPostUserId;
    
        return $this;
    }

    /**
     * Get firstPostUserId
     *
     * @return integer 
     */
    public function getFirstPostUserId()
    {
        return $this->firstPostUserId;
    }

    /**
     * Set forum
     *
     * @param \Project\ForumBundle\Entity\Forum $forum
     * @return Thread
     */
    public function setForum(\Project\ForumBundle\Entity\Forum $forum = null)
    {
        $this->forum = $forum;
    
        return $this;
    }

    /**
     * Set user
     *
     * @param \Project\UserBundle\Entity\User $user
     * @return Thread
     */
    public function setUser(\Project\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Project\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}