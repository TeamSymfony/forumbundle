<?php

namespace Project\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Forum
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Project\ForumBundle\Entity\ForumRepository")
 */
class Forum
{
    public function __construct()
    {
        $now = new \DateTime();

       $this->allowPosting = true;
       $this->discussionCount = 0;
       $this->messageCount = 0;
       $this->allowPosting = true;
       $this->lastPostId = 0;
       $this->lastPostDate = $now;
       $this->lastPostUserId = 0;
       $this->lastPostUsername = 0;
       $this->lastThreadTitle = 0;
       $this->lastThreadId = 0; // pour éviter les erreurs à la création d'un fofo
       $this->displayOrder = 0;
       $this->isLocked = false;
       $this->groupsWithAccess = new \Doctrine\Common\Collections\ArrayCollection();
       $this->threads = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @ORM\OneToMany(targetEntity="Project\ForumBundle\Entity\Thread", mappedBy="forum", cascade={"remove"})
     */
    private $threads;

    /**
     * @ORM\ManyToOne(targetEntity="Project\ForumBundle\Entity\Category", inversedBy="forums")
     */
    private $category;

    /**
     * @ORM\ManyToMany(targetEntity="Project\UserBundle\Entity\Team", mappedBy="forums")
     **/
    private $groupsWithAccess;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="category_node_id ", type="integer")
     */
    private $categoryNodeId;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="discussion_count", type="integer")
     */
    private $discussionCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="message_count", type="integer")
     */
    private $messageCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_post_id", type="integer")
     */
    private $lastPostId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_post_date", type="datetime")
     */
    private $lastPostDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_post_user_id", type="integer")
     */
    private $lastPostUserId;

    /**
     * @var string
     *
     * @ORM\Column(name="last_post_username", type="string", length=255)
     */
    private $lastPostUsername;

    /**
     * @var string
     *
     * @ORM\Column(name="last_thread_title", type="string", length=255)
     */
    private $lastThreadTitle;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_thread_id", type="integer")
     */
    private $lastThreadId;

    /**
     * @var integer
     *
     * @ORM\Column(name="display_order", type="integer")
     */
    private $displayOrder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_locked", type="boolean")
     */
    private $isLocked;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Forum
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set categoryNodeId
     *
     * @param integer $categoryNodeId
     * @return Forum
     */
    public function setCategoryNodeId($categoryNodeId)
    {
        $this->categoryNodeId = $categoryNodeId;
    
        return $this;
    }

    /**
     * Get categoryNodeId
     *
     * @return integer 
     */
    public function getCategoryNodeId()
    {
        return $this->categoryNodeId;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Forum
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set discussionCount
     *
     * @param integer $discussionCount
     * @return Forum
     */
    public function setDiscussionCount($discussionCount)
    {
        $this->discussionCount = $discussionCount;
    
        return $this;
    }

    /**
     * Get discussionCount
     *
     * @return integer 
     */
    public function getDiscussionCount()
    {
        return $this->discussionCount;
    }

    /**
     * Set messageCount
     *
     * @param integer $messageCount
     * @return Forum
     */
    public function setMessageCount($messageCount)
    {
        $this->messageCount = $messageCount;
    
        return $this;
    }

    /**
     * Get messageCount
     *
     * @return integer 
     */
    public function getMessageCount()
    {
        return $this->messageCount;
    }

    /**
     * Set lastPostId
     *
     * @param integer $lastPostId
     * @return Forum
     */
    public function setLastPostId($lastPostId)
    {
        $this->lastPostId = $lastPostId;
    
        return $this;
    }

    /**
     * Get lastPostId
     *
     * @return integer 
     */
    public function getLastPostId()
    {
        return $this->lastPostId;
    }

    /**
     * Set lastPostDate
     *
     * @param \DateTime $lastPostDate
     * @return Forum
     */
    public function setLastPostDate($lastPostDate)
    {
        $this->lastPostDate = $lastPostDate;
    
        return $this;
    }

    /**
     * Get lastPostDate
     *
     * @return \DateTime 
     */
    public function getLastPostDate()
    {
        return $this->lastPostDate;
    }

    /**
     * Set lastPostUserId
     *
     * @param integer $lastPostUserId
     * @return Forum
     */
    public function setLastPostUserId($lastPostUserId)
    {
        $this->lastPostUserId = $lastPostUserId;
    
        return $this;
    }

    /**
     * Get lastPostUserId
     *
     * @return integer 
     */
    public function getLastPostUserId()
    {
        return $this->lastPostUserId;
    }

    /**
     * Set lastPostUsername
     *
     * @param string $lastPostUsername
     * @return Forum
     */
    public function setLastPostUsername($lastPostUsername)
    {
        $this->lastPostUsername = $lastPostUsername;
    
        return $this;
    }

    /**
     * Get lastPostUsername
     *
     * @return string 
     */
    public function getLastPostUsername()
    {
        return $this->lastPostUsername;
    }

    /**
     * Set lastThreadTitle
     *
     * @param string $lastThreadTitle
     * @return Forum
     */
    public function setLastThreadTitle($lastThreadTitle)
    {
        $this->lastThreadTitle = $lastThreadTitle;
    
        return $this;
    }

    /**
     * Get lastThreadTitle
     *
     * @return string 
     */
    public function getLastThreadTitle()
    {
        return $this->lastThreadTitle;
    }

    /**
     * Set displayOrder
     *
     * @param integer $displayOrder
     * @return Forum
     */
    public function setDisplayOrder($displayOrder)
    {
        $this->displayOrder = $displayOrder;
    
        return $this;
    }

    /**
     * Get displayOrder
     *
     * @return integer 
     */
    public function getDisplayOrder()
    {
        return $this->displayOrder;
    }

    /**
     * Set isLocked
     *
     * @param boolean $isLocked
     * @return Forum
     */
    public function setIsLocked($isLocked)
    {
        $this->isLocked = $isLocked;
    
        return $this;
    }

    /**
     * Get isLocked
     *
     * @return boolean 
     */
    public function getIsLocked()
    {
        return $this->isLocked;
    }

    /**
     * Set category
     *
     * @param \Project\ForumBundle\Entity\Category $category
     * @return Forum
     */
    public function setCategory(\Project\ForumBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Project\ForumBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add groupsWithAccess
     *
     * @param \Project\UserBundle\Entity\Team $groupsWithAccess
     * @return Forum
     */
    public function addGroupsWithAcces(\Project\UserBundle\Entity\Team $groupsWithAccess)
    {
        $this->groupsWithAccess[] = $groupsWithAccess;
    
        return $this;
    }

    /**
     * Remove groupsWithAccess
     *
     * @param \Project\UserBundle\Entity\Team $groupsWithAccess
     */
    public function removeGroupsWithAcces(\Project\UserBundle\Entity\Team $groupsWithAccess)
    {
        $this->groupsWithAccess->removeElement($groupsWithAccess);
    }

    /**
     * Get groupsWithAccess
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroupsWithAccess()
    {
        return $this->groupsWithAccess;
    }

    /**
     * Set threads
     *
     * @param \Project\ForumBundle\Entity\Thread $threads
     * @return Forum
     */
    public function setThreads(\Project\ForumBundle\Entity\Thread $threads = null)
    {
        $this->threads = $threads;
    
        return $this;
    }

    /**
     * Get threads
     *
     * @return \Project\ForumBundle\Entity\Thread 
     */
    public function getThreads()
    {
        return $this->threads;
    }

    /**
     * Add threads
     *
     * @param \Project\ForumBundle\Entity\Thread $threads
     * @return Forum
     */
    public function addThread(\Project\ForumBundle\Entity\Thread $threads)
    {
        $this->threads[] = $threads;
    
        return $this;
    }

    /**
     * Remove threads
     *
     * @param \Project\ForumBundle\Entity\Thread $threads
     */
    public function removeThread(\Project\ForumBundle\Entity\Thread $threads)
    {
        $this->threads->removeElement($threads);
    }

    /**
     * Set lastThreadId
     *
     * @param integer $lastThreadId
     * @return Forum
     */
    public function setLastThreadId($lastThreadId)
    {
        $this->lastThreadId = $lastThreadId;
    
        return $this;
    }

    /**
     * Get lastThreadId
     *
     * @return integer 
     */
    public function getLastThreadId()
    {
        return $this->lastThreadId;
    }
}