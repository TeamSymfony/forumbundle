INFORMATION UTILES
======================================

Placement automatique des js/css/img
-----------------------------------------

> php app/console assets:install web

- - -

Dépendance composer à installer et configurer une à une
----------------------------------------------------

> "friendsofsymfony/user-bundle": "dev-master",

> "knplabs/knp-paginator-bundle": "dev-master", 

> "knplabs/knp-time-bundle": "1.1.*@dev", 

> "exercise/htmlpurifier-bundle": "dev-master"
