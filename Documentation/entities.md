Liste des entitées du projet
==================================================

Routes publique
----------------------------------------------

* /

Routes admin
----------------------------------------------

* /admin/categories

- - -

Category
----------------------------------------------

*$groups_with_access <== ManyToMany ==> Categories*


* category_id (ID)
* node_type (string)
* title (string)
* description (string)
* display_order (integer)
* owned_by

- - -

Forum
-----------------------------------

*$categories <== ManyToMany ==> $forums*

*$threads <== ManyToMany ==> $forum*

*$groups_with_access <== ManyToMany ==> Categories*


* forum_id (ID)
* category_node_id <=== a vérifier si dans entité
* title (string)
* description (string)
* discussion_count (integer)
* message_count (integer)
* last_post_id (integer)
* last_post_date (datetime)
* last_post_user_id (integer)
* last_post_username (string)
* last_thread_title (string)
* allow_posting (boolean)
* display_order (integer)
* is_locked (boolean)

- - -

User
----------------------------------------------------

* forum_message_count (integer)
* forum_likes (integer)
* forum_dislikes (integer)
* forum_rating_ratio (float)
    
- - -

Thread
-----------------------------------------------

*$user <== OneToMany ==> Thread*

*$forum  <== OneToMany ==> $threads*


* thread_id (ID)
* category_node_id <=== a vérifier si dans entité
* title (string)
* username (string)
* is_locked (boolean)<===== a vérifier si dans entité et si is_open est bien supprimé
* date_creation (datetime)
* reply_count (integer)
* view_count (integer)
* sticky (boolean)
* last_post_date (datetime)
* last_post_id (integer)
* last_post_user_id (integer)
* last_post_username (integer)
* first_post_id (integer)
* first_post_likes (integer)

- - -

Message
-------------------------------------------

*$user <== ManyToOne ==> Messages*

*$thread <== ManyToOne ==> Messages*


* message_id (ID) <== ManyToOne ==> thread_id
* user_id
* content (string)
* date_creation (datetime)
* date_update (datetime)
* published (boolean)
* likes (integer)
* dislikes (integer)
* ip (string)

- - -

Rating
-------------------------------

*$user <== ManyToOne ==> Messages (ID)*

*$message <== ManyToOne ==> rating (ID)*


* likes (boolean) true ==> j’aime //// false ⇒ j’aime pas

- - -